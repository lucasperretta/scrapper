const cron = require('node-cron')
const fetch = require('node-fetch')
const fs = require('fs')

exports.runScrape = async (name, errorCountNotification) => {
    const dataPath = 'data/' + name + '/'
    if (!fs.existsSync(dataPath)) {
        if (!fs.existsSync('data')) fs.mkdirSync('data')
        fs.mkdirSync(dataPath)
    }
    try {
        await require('./scrapes/' + name + '.js')(dataPath)
            .catch((error) => {
                console.log(error)
                throw error
            })
        console.log('Executed Script "' + name + '" on ' + new Date().toLocaleString('es-AR'))
        await updateErrorInfo(name, dataPath, errorCountNotification, false)
    } catch (e) {
        console.log('FAILED to execute Script "' + name + '" on ' + new Date().toLocaleString('es-AR'))
        await updateErrorInfo(name, dataPath, errorCountNotification, true)
    }
}

async function updateErrorInfo(script, dataPath, errorCountNotification, failed) {
    if (failed) {
        if (errorCountNotification === null) return
        let previousErrors = 0
        if (fs.existsSync(dataPath + '_errors')) {
            previousErrors = parseInt(fs.readFileSync(dataPath + '_errors', 'utf-8'))
            fs.unlinkSync(dataPath + '_errors')
        }
        previousErrors++
        fs.appendFileSync(dataPath + '_errors', previousErrors.toString())
        if (previousErrors % errorCountNotification === 0 && previousErrors <= errorCountNotification * 5) {
            console.log('--- FATAL ERROR ON SCRIPT ' + script + '.js')
            await exports.sendFCMNotification('Error ' + script + '.js', 'La ejecución del script ' + script + '.js está fallando\nNotificado después de ' + errorCountNotification + ' ' + (errorCountNotification === 1 ? 'ejecución' : 'ejecuciónes') + ' fallidas')
        }
    } else if (fs.existsSync(dataPath + '_errors')) {
        fs.unlinkSync(dataPath + '_errors')
    }
}

exports.scheduleScrape = (expression, name, errorCountNotification) => {
    cron.schedule(expression, () => {
        exports.runScrape(name, errorCountNotification)
    }, undefined)
}

exports.sendFCMNotification = async (title, body, priority) => {
    if (!priority) {
        priority = 10
    }
    await fetch('https://fcm.googleapis.com/fcm/send', {
        method: 'POST',
        body: JSON.stringify({
            to: process.env.FCM_USER_TOKEN,
            notification: {
                title: title,
                body: body,
                sound: "sound"
            },
            priority: priority
        }),
        headers: {
            'Authorization': 'key=' + process.env.FCM_SERVER_TOKEN,
            'Content-Type': 'application/json'
        }
    })
}

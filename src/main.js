require('dotenv').config()
const scheduleScrape = require('./utilities').scheduleScrape

// Checking if the call requires just one Scrape to be executed
if (process.argv[2]) { require('./utilities').runScrape(process.argv[2], null); return; }

// From this line goes all the Scheduled Scrapes.....
scheduleScrape('*/15 * * * *', 'comafi', 8)

// Until this line.....
console.log('System Ready')

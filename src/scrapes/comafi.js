const puppeteer = require('puppeteer');
const fs = require('fs')
const utilities = require('../utilities')

module.exports = async (dataPath) => {
    let lastStatus = null
    if (fs.existsSync(dataPath + 'status')) {
        lastStatus = fs.readFileSync(dataPath + 'status', 'utf-8')
        fs.unlinkSync(dataPath + 'status')
    }

    const browser = await puppeteer.launch({
        headless: true,
        executablePath: process.env.CHROMIUM_PATH ? process.env.CHROMIUM_PATH : undefined,
        args: ["--no-sandbox"]
    });
    try {
        let status = await execute(browser)
        if (lastStatus != null && status !== lastStatus) {
            await utilities.sendFCMNotification('Banco Comafi', 'El estado de tu cuenta cambió de $' + lastStatus +' a $' + status)
            console.log('Change Detected')
        }
        fs.appendFileSync(dataPath + 'status', status)
    } catch (e) {
        throw e
    }
    await browser.close();
    try {
        require("child_process").exec("killall chrome", (error, stdout, stderr) => {})
    } catch(e) {}
}

async function execute(browser) {
    const page = await browser.newPage();
    await page.goto('https://hb.comafi.com.ar/homebank/HBI.do');
    await page.waitForSelector("iframe");

    let frame = (await page.frames()).find(f => f.url().indexOf("loginUnificado") > -1)
    await frame.evaluate(({dni, user, pass}) => {
        window.passedDni = dni
        window.passedUser = user
        window.passedPass = pass
    }, {
        dni: process.env.COMAFI_DNI,
        user: process.env.COMAFI_USUARIO,
        pass: process.env.COMAFI_PASSWORD
    })

    await frame.$eval('#nroDocumento', (element) => {
        element.value = window.passedDni
    })
    await frame.$eval('#claveAux', (element) => {
        element.value = window.passedPass
    })
    await frame.$eval('#usuarioAux', (element) => {
        element.value = window.passedUser
    })

    await frame.click('#btnContinuar')
    await page.waitForNavigation()
    frame = (await page.frames()).find(f => f.url().indexOf("posicionConsolidada") > -1)
    let valor = frame.evaluate(function () {
        return document.querySelector('.itemTabla:nth-child(4) > td:nth-child(8)').innerText.trim()
    });

    await page.goto('https://hb.comafi.com.ar/homebank/salir.do')

    return valor;
}
